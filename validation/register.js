const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateRegisterInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : '';
  data.phone = !isEmpty(data.phone) ? data.phone : '';
  data.email = !isEmpty(data.email) ? data.email : '';
  data.password = !isEmpty(data.password) ? data.password : '';
  data.password2 = !isEmpty(data.password2) ? data.password2 : '';

  if (!Validator.isLength(data.name, { min: 2, max: 30 })) {
    errors.name = "Имя должно иметь не менее 2х символов";
  }

  if (Validator.isEmpty(data.name)) {
    errors.name = "Необходимо ввести имя";
  }

  if (!Validator.isMobilePhone(data.phone, 'ru-RU', { strictMode: true })) {
    errors.phone = "Неизвестный формат номера телефона";
  }

  if (Validator.isEmpty(data.phone)) {
    errors.phone = "Необходимо ввести номер телефона";
  }

  if (!Validator.isEmail(data.email)) {
    errors.email = "Неизвестный формат почты";
  }

  if (Validator.isEmpty(data.email)) {
    errors.email = "Необходимо ввести почту";
  }

  if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = "Длина пароля должна быть от 6 до 30 символов";
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = "Необходимо ввести пароль";
  }

  if (Validator.isEmpty(data.password2)) {
    errors.password2 = "Сюда также необходимо ввести пароль";
  }

  if (!Validator.equals(data.password, data.password2)) {
    errors.password2 = "Введенные пароли не совпадают";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
}