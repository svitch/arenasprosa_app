const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateTaskInput(data) {
  let errors = {};

  data.title = !isEmpty(data.title) ? data.title : '';
  data.text = !isEmpty(data.text) ? data.text : '';

  if (!Validator.isLength(data.title, { min: 8, max: 80 })) {
    errors.title = 'Title needs to be between 8 and 80 characters';
  }

  if (Validator.isEmpty(data.title)) {
    errors.title = 'Profile title is required';
  }

  if (Validator.isEmpty(data.category)) {
    errors.category = 'Category field is required';
  }

  if (Validator.isEmpty(data.subcategory)) {
    errors.subcategory = 'Subcategory field is required';
  }

  if (!Validator.isCurrency(data.price, {
    // symbol: '$',
    // require_symbol: false,
    // allow_space_after_symbol: false,
    // symbol_after_digits: false,
    allow_negatives: false,
    // parens_for_negatives: false,
    // negative_sign_before_digits: false,
    // negative_sign_after_digits: false,
    // allow_negative_sign_placeholder: false,
    thousands_separator: ' ',
    decimal_separator: '.',
    allow_decimal: false,
    // require_decimal: false,
    // digits_after_decimal: [2],
    allow_space_after_digits: false
  })) {
    errors.price = 'Price field is required';
  }

  if (Validator.isEmpty(data.price)) {
    errors.price = 'Price field is required';
  }

  if (Validator.isEmpty(data.deadline)) {
    errors.deadline = 'Deadline date field is required';
  }

  if (Validator.isEmpty(data.city)) {
    errors.city = 'City date field is required';
  }

  if (Validator.isEmpty(data.address)) {
    errors.address = 'Address date field is required';
  }

  if (!Validator.isLength(data.content, { min: 10, max: 400 })) {
    errors.content = "Content must be between 10 and 300 characters";
  }

  if (Validator.isEmpty(data.content)) {
    errors.content = "Content field is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
}