import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextAreaFieldGroup';
import SelectListGroup from '../common/SelectListGroup';
import CheckboxFieldGroup from '../common/CheckboxFieldGroup';
import { createProfile } from '../../actions/profileActions';

class CreateProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displaySocialInputs: false,
      handle: '',
      company: '',
      website: '',
      location: '',
      status: '',
      skills: '',
      bio: '',
      youtube: '',
      twitter: '',
      facebook: '',
      instagram: '',
      agree: false,
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(event) {
    event.preventDefault();

    const profileData = {
      handle: this.state.handle,
      company: this.state.company,
      website: this.state.website,
      location: this.state.location,
      status: this.state.status,
      skills: this.state.skills,
      bio: this.state.bio,
      youtube: this.state.youtube,
      twitter: this.state.twitter,
      facebook: this.state.facebook,
      instagram: this.state.instagram
    };

    this.props.createProfile(profileData, this.props.history);
  }

  onChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    const { errors, displaySocialInputs } = this.state;

    let socialInputs;

    if (displaySocialInputs) {
      socialInputs = (
        <div>

          <TextFieldGroup
            name="youtube"
            placeholder="youtube"
            value={this.state.youtube}
            label="youtube"
            error={errors.youtube}
            onChange={this.onChange}
          />

          <TextFieldGroup
            name="twitter"
            placeholder="twitter"
            value={this.state.twitter}
            label="twitter"
            error={errors.twitter}
            onChange={this.onChange}
          />

          <TextFieldGroup
            name="facebook"
            placeholder="facebook"
            value={this.state.facebook}
            label="facebook"
            error={errors.facebook}
            onChange={this.onChange}
          />

          <TextFieldGroup
            name="instagram"
            placeholder="instagram"
            value={this.state.instagram}
            label="instagram"
            error={errors.instagram}
            onChange={this.onChange}
          />

        </div>
      );
    }

    // Select options for status
    const options = [
      { label: '* Выберите вашу специализацию', value: 0 },
      { label: 'Developer', value: 'Developer' },
      { label: 'Designer', value: 'Designer' },
      { label: 'Other', value: 'Other' }
    ];

    return (
      <main id="main">
        <div className="page-header">
          <div className="container">
            <h1 className="page-heading">
              Личный кабинет <span className="separator">⟩</span> <small>Личные данные</small>
            </h1>
          </div>
        </div>
        <div className="section">
          <div className="container">
            <div className="row">
              <div className="col-md-3 col-lg-2">

                <ul className="list-group">
                  <li className="list-group-item active"><a className="list-group-link" href="account-index.html">Личные данные</a></li>
                  <li className="list-group-item"><a className="list-group-link" href="account-profile.html">Мой профиль</a></li>
                  <li className="list-group-item"><a className="list-group-link" href="account-favorites.html">Избранное</a></li>
                  <li className="list-group-item"><a className="list-group-link" href="account-changepassword.html">Смена пароля</a></li>
                  <li className="list-group-item"><a className="list-group-link" href="account-settings.html">Настройки</a></li>
                  <li className="list-group-item"><a className="list-group-link" href="index.html">Выйти</a></li>
                </ul>

              </div>
              <div className="col-md-6 col-lg-8">
                <div className="panel">
                  <div className="panel-body">
                    <div className="form">
                      <form onSubmit={this.onSubmit} className="needs-validation" noValidate>

                        <TextFieldGroup
                          name="name"
                          placeholder="* Ваше имя"
                          value={this.state.name}
                          label="Ваше имя"
                          error={errors.name}
                          onChange={this.onChange}
                        />

                        <TextFieldGroup
                          name="handle"
                          placeholder="* Адрес профиля"
                          value={this.state.handle}
                          label="Адрес профиля"
                          error={errors.handle}
                          onChange={this.onChange}
                          info="Уникальный адрес вашего профиля"
                        />

                        <SelectListGroup
                          name="status"
                          placeholder="* Профессия"
                          value={this.state.status}
                          options={options}
                          label="Профессия"
                          error={errors.status}
                          onChange={this.onChange}
                        />

                        <TextFieldGroup
                          name="company"
                          placeholder="Компания"
                          value={this.state.company}
                          label="Компания"
                          error={errors.company}
                          onChange={this.onChange}
                        />

                        <TextFieldGroup
                          name="website"
                          placeholder="Веб-сайт"
                          value={this.state.website}
                          label="Веб-сайт"
                          error={errors.website}
                          onChange={this.onChange}
                        />

                        <TextFieldGroup
                          name="location"
                          placeholder="Адрес проживания"
                          value={this.state.location}
                          label="Адрес проживания"
                          error={errors.location}
                          onChange={this.onChange}
                        />

                        <TextFieldGroup
                          name="skills"
                          placeholder="* Ваши навыки"
                          value={this.state.skills}
                          label="Ваши навыки"
                          error={errors.skills}
                          onChange={this.onChange}
                          info="Ваши навыки, через запятую"
                        />

                        <TextAreaFieldGroup
                          name="bio"
                          placeholder="* Биография"
                          value={this.state.bio}
                          label="Биография"
                          rows="6"
                          error={errors.bio}
                          onChange={this.onChange}
                          info="Немного о себе"
                        />

                        <div className="form-group">
                          <button
                            type="button"
                            onClick={() => {
                              this.setState(prevState => ({
                                displaySocialInputs: !prevState.displaySocialInputs
                              }));
                            }}
                            className="btn btn-light"
                          >
                            Add Social Network Links
                          </button>
                          <span className="text-muted">Optional</span>
                        </div>
                        {socialInputs}

                        <CheckboxFieldGroup
                          name="agree"
                          checked={this.state.agree}
                          label="Согласен с правилами пользования"
                          error={errors.agree}
                          onChange={this.onChange}
                        />

                        <div className="form-footer">
                          <button type="submit" disabled={!this.state.agree} className="btn btn-primary btn-wide rip">Сохранить</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    )
  }
}

CreateProfile.propTypes = {
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  errors:  state.errors
});

export default connect(mapStateToProps, { createProfile })(withRouter(CreateProfile));