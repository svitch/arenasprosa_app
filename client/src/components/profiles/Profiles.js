import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import Spinner from '../common/Spinner';
import  ProfileItem from './ProfileItem';
import { getProfiles } from '../../actions/profileActions';

class Profiles extends Component {
  componentDidMount() {
    this.props.getProfiles();
  }

  render() {
    const { profiles, loading } = this.props.profile;
    let profileItems;

    if (profiles === null || loading) {
      profileItems = <Spinner />;
    } else {
      if (profiles.length > 0) {
        profileItems = profiles.map(profile => (
          <ProfileItem key={profile._id} profile={profile} />
        ));
      } else {
        profileItems = <h4>No profiles found</h4>;
      }
    }

    return (
      <main id="main">
        <div className="page-header">
          <div className="container">
            <h1 className="page-heading">
              Исполнители
            </h1>
          </div>
        </div>
        <div className="section">
          <div className="container">

            <div className="row">
              <div className="col-md-9">

                {profileItems}

              </div>
              <div className="col-md-3">
                <div className="side-block adv"></div>
                <div className="side-block adv"></div>
                <div className="side-block adv"></div>
              </div>
            </div>

          </div>
        </div>
      </main>
    )
  }
}

Profiles.propTypes = {
  getProfiles: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  errors:  state.errors
});

export default connect(mapStateToProps, { getProfiles })(Profiles);