import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import isEmpty from '../../validation/is-empty';

class ProfileItem extends Component {
  render() {
    const { profile } = this.props;

    return (
      <div className="panel">
        <div className="panel-body">
          <Link className="panel-title" to={`/profile/${profile.handle}`}>{profile.user.name}</Link>
          <p className="panel-text">{profile.bio}</p>
          <div className="panel-props row row-sm">
            <div className="panel-prop col-12 col-sm-auto">{profile.status}</div>
            {isEmpty(profile.location) ? null : (<div className="panel-prop col-12 col-sm-auto">{profile.location}</div>) }
            <div className="panel-prop col-12 col-sm-auto">{profile.skills.slice(0,4).join(', ')
            /*
            .map((skill, index) => (
              <span key={index}>{skill}</span>
            ))
            */
            }</div>
          </div>
        </div>
      </div>
    )
  }
}

ProfileItem.propTypes = {
  profile: PropTypes.object.isRequired
}

export default ProfileItem;