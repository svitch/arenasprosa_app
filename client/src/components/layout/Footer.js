import React from 'react';
import { Link } from 'react-router-dom';

const Footer = () => {
  return (
    <footer id="footer">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-12 col-md">
            <nav className="nav justify-content-center justify-content-md-start">
              <Link className="nav-link footer-link" to="/about">О сервисе</Link>
              <Link className="nav-link footer-link" to="/feedback">Обратная связь</Link>
              <Link className="nav-link footer-link" to="/agreement">Пользовательское соглашение</Link>
              <Link className="nav-link footer-link" to="/help">Помощь</Link>
            </nav>
          </div>
          <div className="col-12 col-md-auto text-center copyright">
            &copy; { new Date().getFullYear()} &laquo;Арена Спроса&raquo;
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer;