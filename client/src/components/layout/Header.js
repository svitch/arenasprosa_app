import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions/authActions';
import { clearCurrentProfile } from '../../actions/profileActions';

class Header extends Component {
  onLogoutClick(event) {
    event.preventDefault();
    this.props.clearCurrentProfile();
    this.props.logoutUser();
  }

  render() {
    const { isAuthenticated, user } = this.props.auth;

    const authLinks = (
      <div className="col-auto d-none d-md-block">
        <nav className="nav menu">
          <Link className="nav-link menu-link" to="/dashboard">{user.name}</Link>
          <button type="button" className="nav-link menu-link" onClick={this.onLogoutClick.bind(this)}>Выход</button>
        </nav>
      </div>
    );

    const guestLinks = (
      <div className="col-auto d-none d-md-block">
        <nav className="nav menu">
          <Link className="nav-link menu-link" to="/login">Вход</Link>
          <Link className="nav-link menu-link" to="/register">Регистрация</Link>
        </nav>
      </div>
    );

    return (
      <header id="header">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-auto">
              <Link id="logo" to="/">
                <img src="/images/logo.svg" alt="logo" />
              </Link>
            </div>
            <div className="col d-none d-md-block">
              <nav className="menu nav">
                <Link className="nav-link menu-link" to="/tasks">Задания</Link>
                <Link className="nav-link menu-link" to="/executors">Исполнители</Link>
                <Link className="nav-link menu-link" to="/about">О сервисе</Link>
              </nav>
            </div>
            {isAuthenticated ? authLinks : guestLinks}
          </div>
        </div>
      </header>
    )
  }
}

Header.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps, { logoutUser, clearCurrentProfile })(Header);