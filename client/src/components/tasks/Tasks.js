import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { getTasks } from '../../actions/taskActions';
import TaskFeed from './TaskFeed';
import Spinner from '../common/Spinner';

class Tasks extends Component {
  componentDidMount() {
    this.props.getTasks();
  }

  render() {
    const { tasks, loading } = this.props.task;
    let taskContent;

    if (tasks === null || loading) {
      taskContent = <Spinner />;
    } else {
      taskContent = <TaskFeed tasks={tasks} />;
    }

    return (
      <main id="main">
        <div className="page-header">
          <div className="container">
            <h1 className="page-heading">
              Задания
            </h1>
          </div>
        </div>
        <div className="section">
          <div className="container">

            <div className="row">
              <div className="col-md-9">

                {taskContent}

              </div>
              <div className="col-md-3">
                <div className="side-block">
                  <Link to="/tasks/new" className="btn btn-primary btn-block  btn-lg">Создать задание</Link>
                </div>
                <div className="side-block adv"></div>
                <div className="side-block adv"></div>
                <div className="side-block adv"></div>
              </div>
            </div>

          </div>
        </div>
      </main>
    )
  }
}

Tasks.propTypes = {
  getTasks: PropTypes.func.isRequired,
  task: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  task: state.task,
  errors:  state.errors
});

export default connect(mapStateToProps, { getTasks })(Tasks);