import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import isEmpty from '../../validation/is-empty';

class TaskItem extends Component {
  render() {
    const { task, auth } = this.props;

    return (
      <div className="panel">
        <div className="panel-body">
          <Link className="panel-title" to={`/tasks/${task._id}`}>{task.title}</Link>
          <p className="panel-text">{task.content}</p>
          <div className="panel-props row row-sm">
            <div className="panel-prop col-12 col-sm-auto">{task.category}</div>
          </div>
        </div>
      </div>
    )
  }
}

TaskItem.propTypes = {
  task: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps)(TaskItem);