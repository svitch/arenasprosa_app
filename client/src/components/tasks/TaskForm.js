import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SelectListGroup from '../common/SelectListGroup';
import TextFieldGroup from '../common/TextFieldGroup';
import TextAreaFieldGroup from '../common/TextAreaFieldGroup';
import CheckboxFieldGroup from '../common/CheckboxFieldGroup';
import { addTask } from '../../actions/taskActions';

class TaskForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      category: '',
      subcategory: '',
      price: '',
      deadline: '',
      city: '',
      address: '',
      content: '',
      agree: false,
      errors: {}
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  componentWillReceiveProps(newProps) {
    // Error checking below input
    if (newProps.errors) {
      this.setState({ errors: newProps.errors });
    }
  }

  onSubmit(event) {
    event.preventDefault();

    const { user } = this.props.auth;

    const newTask = {
      title: this.state.title,
      category: this.state.category,
      subcategory: this.state.subcategory,
      price: this.state.price,
      deadline: this.state.deadline,
      city: this.state.city,
      address: this.state.address,
      content: this.state.content,
      name: user.name,
      avatar: user.avatar
    };

    this.props.addTask(newTask);
    this.setState({ text: '' });
  }

  render() {
    const { errors } = this.state;

    // Categories
    const categories = [
      { label: '* Выберите категорию', value: 0 },
      { label: 'Аренда', value: '1' },
      { label: 'Строительство', value: '2' },
      { label: 'Грузоперевозки', value: '3' },
      { label: 'Обслуживание', value: '4' },
      { label: 'Красота и здоровье', value: '5' },
      { label: 'Развлечения', value: '6' },
      { label: 'Фото/видеоуслуги', value: '7' },
      { label: 'Репетиторство', value: '8' },
      { label: 'Юридическая помощь', value: '9' },
      { label: 'Компьютерные технологии', value: '10' },
      { label: 'Вебсайты', value: '11' },
      { label: 'Дизайн', value: '12' },
      { label: 'Прочие услуги', value: '13' }
    ];

    return (
      <main id="main">
        <div className="page-header">
          <div className="container">
            <h1 className="page-heading">
              Задания <span className="separator">⟩</span> <small>Новое задание</small>
            </h1>
          </div>
        </div>
        <div className="section">
          <div className="container">
            <div className="row">

              <div className="col-9">
                <div className="panel">
                  <div className="panel-body">
                    <div className="form">
                      <form onSubmit={this.onSubmit} className="needs-validation" noValidate>

                        <TextFieldGroup
                          name="title"
                          placeholder="Заголовок задания"
                          value={this.state.title}
                          label="Заголовок задания"
                          error={errors.title}
                          onChange={this.onChange}
                        />

                        <SelectListGroup
                          name="category"
                          value={this.state.category}
                          options={categories}
                          label="Категория"
                          error={errors.category}
                          onChange={this.onChange}
                        />

                        <SelectListGroup
                          name="subcategory"
                          value={this.state.subcategory}
                          options={categories}
                          label="Подкатегория"
                          error={errors.subcategory}
                          onChange={this.onChange}
                        />

                        <TextFieldGroup
                          name="price"
                          placeholder="Стоимость задания"
                          value={this.state.price}
                          label="Стоимость задания"
                          error={errors.price}
                          onChange={this.onChange}
                        />

                        <TextFieldGroup
                          name="deadline"
                          type="date"
                          placeholder="Начало работ"
                          value={this.state.deadline}
                          label="Начало работ"
                          error={errors.deadline}
                          onChange={this.onChange}
                        />

                        <TextFieldGroup
                          name="city"
                          placeholder="Город"
                          value={this.state.city}
                          label="Город"
                          error={errors.city}
                          onChange={this.onChange}
                        />

                        <TextFieldGroup
                          name="address"
                          placeholder="Адрес"
                          value={this.state.address}
                          label="Адрес"
                          error={errors.address}
                          onChange={this.onChange}
                        />

                        <TextAreaFieldGroup
                          name="content"
                          placeholder="* Описание"
                          value={this.state.content}
                          label="Описание"
                          rows="6"
                          error={errors.content}
                          onChange={this.onChange}
                          info="Описание задания"
                        />

                        <CheckboxFieldGroup
                          name="agree"
                          checked={this.state.agree}
                          label="Согласен с правилами пользования"
                          error={errors.agree}
                          onChange={this.onChange}
                        />

                        <div className="form-group">
                          <button type="submit" disabled={!this.state.agree} className="btn btn-primary btn-wide rip">Опубликовать</button>
                        </div>

                        <p className="text-muted">Нажимая «Опубликовать», вы соглашаетесь с правилами платёжного сервиса «Единая касса — Безопасная Сделка» и сервиса «Сделка без риска». Ваш банк может взимать дополнительную комиссию.</p>

                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-3">

                <div className="side-block">
                  <p>Опишите пожелания и детали, чтобы исполнители лучше оценили вашу задачу</p>
                  <p>Мы быстро оповестим исполнителей о вашей заявке.</p>
                  <p>Заинтересованные исполнители предложат вам свои услуги.</p>
                  <p>Выберите подходящее для вас предложение по цене или рейтингу исполнителя.</p>
                </div>

              </div>

            </div>
          </div>
        </div>

      </main>
    )
  }
}

TaskForm.propTypes = {
  addTask: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { addTask })(TaskForm);
