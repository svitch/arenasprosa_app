import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { loginUser } from '../../actions/authActions';
import TextFieldGroup from '../common/TextFieldGroup';
import CheckboxFieldGroup from '../common/CheckboxFieldGroup';

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      remember: false,
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    // If user is already logged, redirect to dashboard
    if(this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard');
    }
  }

  componentWillReceiveProps(nextProps) {
    // If user is already logged, redirect to dashboard
    if(nextProps.auth.isAuthenticated) {
      this.props.history.push('/dashboard');
    }

    if(nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(event) {
    event.preventDefault();

    const userData = {
      email: this.state.email,
      password: this.state.password
    }

    this.props.loginUser(userData);
  }

  onChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    const { errors } = this.state;

    return (
      <section id="auth" className="section justify-content-center">
        <div className="container">
          <h1 className="page-heading text-center">Авторизация</h1>
          <div className="form form-short">
            <form onSubmit={this.onSubmit} className="needs-validation" noValidate>

              <TextFieldGroup
                name="email"
                placeholder="Электронная почта"
                value={this.state.email}
                label="Электронная почта"
                error={errors.email}
                type="email"
                onChange={this.onChange}
              />

              <TextFieldGroup
                name="password"
                placeholder="Пароль"
                value={this.state.password}
                label="Пароль"
                error={errors.password}
                type="password"
                onChange={this.onChange}
              />

              <CheckboxFieldGroup
                name="remember"
                checked={this.state.remember}
                label="Запомнить меня"
                error={errors.remember}
                onChange={this.onChange}
              />

              <div className="form-button">
                <button type="submit" className="btn btn-primary btn-wide rip">Войти</button>
              </div>
              <div className="text-center">
                <small>Ещё нет аккаунта?</small>
                <br />
                <Link className="link lead" to="/register">Зарегистрируйтесь</Link>
              </div>
            </form>
          </div>
        </div>
      </section>
    )
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { loginUser })(Login);
