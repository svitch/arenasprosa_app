import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { registerUser } from '../../actions/authActions';
import TextFieldGroup from '../common/TextFieldGroup';
import CheckboxFieldGroup from '../common/CheckboxFieldGroup';

class Register extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      phone: '',
      email: '',
      password: '',
      password2: '',
      agree: false,
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    // If user is already logged, redirect to dashboard
    if(this.props.auth.isAuthenticated) {
      this.props.history.push('/dashboard');
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }

  onSubmit(event) {
    event.preventDefault();

    const newUser = {
      name: this.state.name,
      phone: this.state.phone,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2,
    }

    this.props.registerUser(newUser, this.props.history);
  }

  onChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    const { errors } = this.state;

    return (
      <section id="auth" className="section justify-content-center">
        <div className="container">
          <h1 className="page-heading text-center">Регистрация</h1>
          <div className="form form-short">
            <form onSubmit={this.onSubmit} className="needs-validation" noValidate>

              <TextFieldGroup
                name="name"
                placeholder="Ваше имя"
                value={this.state.name}
                label="Ваше имя"
                error={errors.name}
                onChange={this.onChange}
              />

              <TextFieldGroup
                name="phone"
                placeholder="Номер телефона"
                value={this.state.phone}
                label="Номер телефона"
                error={errors.phone}
                onChange={this.onChange}
              />

              <TextFieldGroup
                name="email"
                placeholder="Электронная почта"
                value={this.state.email}
                label="Электронная почта"
                error={errors.email}
                type="email"
                onChange={this.onChange}
              />

              <TextFieldGroup
                name="password"
                placeholder="Пароль"
                value={this.state.password}
                label="Пароль"
                error={errors.password}
                type="password"
                onChange={this.onChange}
              />

              <TextFieldGroup
                name="password2"
                placeholder="Пароль ещё раз"
                value={this.state.password2}
                label="Подтверждение пароля"
                error={errors.password2}
                type="password"
                onChange={this.onChange}
              />

              <CheckboxFieldGroup
                name="agree"
                checked={this.state.agree}
                label="Согласен с правилами пользования"
                error={errors.agree}
                onChange={this.onChange}
              />

              <div className="form-button">
                <button type="submit" disabled={!this.state.agree} className="btn btn-primary btn-wide rip">Зарегистрироваться</button>
              </div>
              <div className="text-center">
                <small>Есть аккаунт?</small>
                <br />
                <Link className="link lead" to="/login">Авторизуйтесь</Link>
              </div>
            </form>
          </div>
        </div>
      </section>
    )
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { registerUser })(withRouter(Register));