import React, { Component } from 'react';
import isEmpty from '../../validation/is-empty';

class ProfileCompleted extends Component {
  render() {
    const { profile } = this.props;

    return (
      <div className="panel">
        <div className="panel-body">

          <div className="row">
            <div className="col-auto">
              <div className="user-image user-image-lg"></div>
            </div>
            <div className="col">
              <div className="row">
                <div className="col-12">Возраст: 33 года</div>
                {isEmpty(profile.location) ? null : (<div className="col-12">Место проживания: {profile.location}</div>) }
                <div className="col-12">Успешных заданий: 39</div>
                <div className="col-12">просмотров: 689</div>
              </div>
            </div>
            <div className="col-12">
              <div className="profile-description">{profile.bio}</div>
            </div>
          </div>

        </div>
      </div>
    )
  }
}

export default ProfileCompleted;