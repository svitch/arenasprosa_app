import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Spinner from '../common/Spinner';
import { getProfileByHandle } from '../../actions/profileActions';
import ProfileAbout from './ProfileAbout';
import ProfileCompleted from './ProfileCompleted';

class Profile extends Component {
  componentDidMount() {
    if (this.props.match.params.handle) {
      this.props.getProfileByHandle(this.props.match.params.handle);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.profile.profile === null && this.props.profile.loading) {
      this.props.history.push('/not-found');
    }
  }

  render() {
    const { profile, loading } = this.props.profile;
    let profileContent;

    if (profile === null || loading) {
      profileContent = <Spinner />;
    } else {
      profileContent = (
        <div className="row">
          <div className="col-12">
            <Link to="/executors">Back To Profiles</Link>
            <h1>{profile.user.name}</h1>
          </div>
          <div className="col-md-9">
            <ProfileAbout profile={profile} />
          </div>
          <div className="col-md-3 text-center">
            <Link className="btn btn-primary btn-lg btn-wide rip" to="/tasks">Предложить задание</Link>
          </div>
          <div className="col-md-9">
            <ProfileCompleted profile={profile} />
          </div>
        </div>
      );
    }

    return (
      <main id="main">
        <div className="section">
          <div className="container">
            {profileContent}
          </div>
        </div>
      </main>
    )
  }
}

Profile.propTypes = {
  getProfileByHandle: PropTypes.func.isRequired,
  profile: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  profile: state.profile
});

export default connect(mapStateToProps, { getProfileByHandle })(Profile);