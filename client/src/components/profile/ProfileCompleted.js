import React, { Component } from 'react';

class ProfileCompleted extends Component {
  render() {
    return (
      <div>

        <h2 class="heading h5 text-left">Выполненные задания</h2>

        <div class="panel">
          <div class="panel-body">
            <a class="panel-title" href="tasks-detail.html">Починить кондиционер в гараже</a>
            <p class="panel-text">I am looking for an experienced wordpress developer to make changes to an existing automated email that is generated in my website when I register a trainer. I require some files attached to the email and a Youtube video inserted into the content ...</p>
            <div class="panel-props row">
              <div class="panel-prop col-auto panel-date">26.06.2018 10:12</div>
              <div class="panel-prop col-auto">Строительство</div>
              <div class="panel-prop col">просмотров: 689</div>
              <div class="panel-prop col-auto panel-price">10 680 руб.</div>
            </div>
          </div>
        </div>

      </div>
    )
  }
}

export default ProfileCompleted;