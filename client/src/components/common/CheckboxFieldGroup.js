import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

 const CheckboxFieldGroup = ({
   name,
   checked,
   label,
   error,
   onChange
 }) => {
  const checkbox = (
    <label className="custom-control custom-checkbox">
      <input
        type="checkbox"
        className={classnames('custom-control-input', { 'is-invalid': error })}
        name={name}
        id={name}
        checked={checked}
        onChange={onChange}
      />
      <span className="custom-control-label">{label}</span>
      {error && <div className="invalid-feedback">{error}</div>}
    </label>
  );

  let groupContainer;

  if (name === 'remember') {
    groupContainer = (
      <div className="form-group">
        <div className="row">
          <div className="col-6">
            {checkbox}
          </div>
          <div className="col-6 text-right">
            <Link to="/reset">Я забыл пароль</Link>
          </div>
        </div>
      </div>
    );
  } else {
    groupContainer = (
      <div className="form-group">
        {checkbox}
      </div>
    );
  }

  return groupContainer;
};

CheckboxFieldGroup.propTypes = {
  name: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired
}

CheckboxFieldGroup.defaultProps = {
  checked: false
}

export default CheckboxFieldGroup;