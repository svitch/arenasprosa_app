import React from 'react';

const Spinner = () => {
  return (
    <div className="spinner">
      <div className="dot one"></div>
      <div className="dot two"></div>
      <div className="dot three"></div>
    </div>
  )
}

export default Spinner;