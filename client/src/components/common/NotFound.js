import React from 'react';

const NotFound = () => {
  return (
    <main id="main">
      <div className="section">
        <div className="container">
          <h1>Здесь Этого Нет</h1>
          <p>Если вы что-то искали, пожалуйста поищите в другом месте</p>
        </div>
      </div>
    </main>
  )
}

export default NotFound;