import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

 const SelectListGroup = ({
   name,
   value,
   label,
   error,
   info,
   onChange,
   options
 }) => {
  const selectOptions = options.map(option => (
    <option key={option.label} value={option.value}>
      {option.label}
    </option>
  ));
  return (
    <div className="form-group">
      <label className="form-label" htmlFor={name}>{label}</label>
      <select
        className={classnames('form-control', { 'is-invalid': error })}
        name={name}
        id={name}
        value={value}
        onChange={onChange}
      >
        {selectOptions}
      </select>
      {info && <small className="form-text">{info}</small>}
      {error && <div className="invalid-feedback">{error}</div>}
    </div>
  )
};

SelectListGroup.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  error: PropTypes.string,
  info: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.array.isRequired
}

export default SelectListGroup;