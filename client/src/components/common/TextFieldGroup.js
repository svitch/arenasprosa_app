import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

 const TextFieldGroup = ({
   name,
   placeholder,
   value,
   label,
   error,
   info,
   type,
   onChange,
   disabled
 }) => {
  return (
    <div className="form-group">
      <label className="form-label" htmlFor={name}>{label}</label>
      <input
        type={type}
        className={classnames('form-control', { 'is-invalid': error })}
        name={name}
        id={name}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        disabled={disabled}
      />
      {info && <small className="form-text">{info}</small>}
      {error && <div className="invalid-feedback">{error}</div>}
    </div>
  )
};

TextFieldGroup.propTypes = {
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  error: PropTypes.string,
  info: PropTypes.string,
  type: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  disabled: PropTypes.string
}

TextFieldGroup.defaultProps = {
  type: 'text'
}

export default TextFieldGroup;