import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

 const TextAreaFieldGroup = ({
   name,
   placeholder,
   value,
   label,
   rows,
   error,
   info,
   onChange
 }) => {
  return (
    <div className="form-group">
      <label className="form-label" htmlFor={name}>{label}</label>
      <textarea
        className={classnames('form-control', { 'is-invalid': error })}
        name={name}
        id={name}
        rows={rows}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
      />
      {info && <small className="form-text">{info}</small>}
      {error && <div className="invalid-feedback">{error}</div>}
    </div>
  )
};

TextAreaFieldGroup.propTypes = {
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string.isRequired,
  rows: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  error: PropTypes.string,
  info: PropTypes.string,
  onChange: PropTypes.func.isRequired
}

TextAreaFieldGroup.defaultProps = {
  rows: '4'
}

export default TextAreaFieldGroup;