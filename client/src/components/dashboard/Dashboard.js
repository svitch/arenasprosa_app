import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getCurrentProfile, deleteAccount } from '../../actions/profileActions';
import { Link } from 'react-router-dom';
import Spinner from '../common/Spinner';
import DashboardActions from './DashboardActions';

class Dashboard extends Component {
  componentDidMount() {
    this.props.getCurrentProfile();
  }

  onDeleteClick(event) {
    this.props.deleteAccount();
  }
  
  render() {
    const { user } = this.props.auth;
    const { profile, loading } = this.props.profile;

    let dashboardContent;

    if (profile === null || loading) {
      dashboardContent = <Spinner />;
    } else {
      // Check if logged in user has profile data
      if (Object.keys(profile).length > 0) {
        dashboardContent = 
        <div>
          <p className="lead text-muted">
            Welcome
            <Link to={`/profile/${profile.handle}`}>
              {user.name}
            </Link>
          </p>

          <div>
            <button onClick={this.onDeleteClick.bind(this)} className="btn btn-secondary">
              Delete my account
            </button>
          </div>
        </div>;
      } else {
        // User is logged in but has no profile
        dashboardContent = (
          <div>
            <p className="lead text-muted">Welcome {user.name}</p>
            <p>You have not yet setup a profile, please add some info</p>
            <Link to="/create-profile" className="btn btn-primary btn-lg">
              Create Profile
            </Link>
          </div>
        );
      }
    }

    return (
      <main id="main">
        <div className="page-header">
          <div className="container">
            <h1 className="page-heading">
              Личный кабинет <span className="separator">&rang;</span> <small>Личные данные</small>
            </h1>
          </div>
        </div>
        <div className="section">
          <div className="container">
            <div className="row">
              <div className="col-md-3 col-lg-2">

                <DashboardActions />

              </div>
              <div className="col-md-6 col-lg-8">
                {dashboardContent}
              </div>
            </div>
          </div>
        </div>
      </main>
    )
  }
}

Dashboard.propTypes = {
  getCurrentProfile: PropTypes.func.isRequired,
  deleteAccount: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  profile: state.profile,
  auth: state.auth
});

export default connect(mapStateToProps, { getCurrentProfile, deleteAccount })(Dashboard);