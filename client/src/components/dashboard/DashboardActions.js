import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions/authActions';
import { clearCurrentProfile } from '../../actions/profileActions';

class DashboardActions extends Component {
  onLogoutClick(event) {
    event.preventDefault();
    this.props.clearCurrentProfile();
    this.props.logoutUser();
  }

  render() {
    return (
      <ul className="list-group">
        <li className="list-group-item">
          <NavLink className="list-group-link" activeClassName="active" to="/dashboard">Личные данные</NavLink>
        </li>
        <li className="list-group-item">
          <NavLink className="list-group-link" activeClassName="active" to="/dashboard/profile">Мой профиль</NavLink>
        </li>
        <li className="list-group-item">
          <NavLink className="list-group-link" activeClassName="active" to="/dashboard/favorites">Избранное</NavLink>
        </li>
        <li className="list-group-item">
          <NavLink className="list-group-link" activeClassName="active" to="/dashboard/changepassword">Смена пароля</NavLink>
        </li>
        <li className="list-group-item">
          <NavLink className="list-group-link" activeClassName="active" to="/dashboard/settings">Настройки</NavLink>
        </li>
        <li className="list-group-item">
          <button type="button" className="list-group-link" onClick={this.onLogoutClick.bind(this)}>Выйти</button>
        </li>
      </ul>
    )
  }
}

DashboardActions.propTypes = {
  logoutUser: PropTypes.func.isRequired
};

export default connect(null, { logoutUser, clearCurrentProfile })(DashboardActions);