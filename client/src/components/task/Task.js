import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Spinner from '../common/Spinner';
import { getTask } from '../../actions/taskActions';
import TaskDetail from './TaskDetail';

class Task extends Component {
  componentDidMount() {
    this.props.getTask(this.props.match.params.id);
  }

  render() {
    const { task, loading } = this.props.task;
    let taskContent;

    if (task === null || loading || Object.keys(task).length === 0) {
      taskContent = <Spinner />;
    } else {
      taskContent = (
        <div>
          <TaskDetail task={task} showActions={false} />
        </div>
      );
    }

    return (
      <div>
        {taskContent}
      </div>
    )
  }
}

Task.propTypes = {
  getTask: PropTypes.func.isRequired,
  task: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  task: state.task
});

export default connect(mapStateToProps, { getTask })(Task);
