import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import isEmpty from '../../validation/is-empty';

class TaskDetail extends Component {
  render() {
    const { task, auth } = this.props;

    return (
      <main id="main">
        <div className="page-header">
          <div className="container">
            <h1 className="page-heading">
              Задание <span className="separator">⟩</span> <small>{task.category}</small> <span className="separator">⟩</span> <small>{task.subcategory}</small>
            </h1>
          </div>
        </div>
        <div className="section">
          <div className="container">
            <div className="row">

              <div className="col-md-9 col-lg-9">
                <h1 className="task-heading">{task.title}</h1>
              </div>
              <div className="col-md-3 col-lg-3">
                <div className="task-price">{task.price} руб.</div>
              </div>

              <div className="col-md-9">

                <div id="task-details" className="panel">
                  <div className="panel-body">
                 
                    <div className="panel-props row">
                      <div className="panel-prop col-auto panel-status">Открыто</div>
                      <div className="panel-prop col-auto">Создано 58 мин. назад</div>
                      <div className="panel-prop col">просмотров: 689</div>
                    </div>                

                    <div className="panel-details">{task.content}</div>

                    <div className="row">
                      <span className="col-6">
                        <dl className="deflist">
                          <dt className="deflist-term">Категория</dt>
                          <dd className="deflist-definition">{task.category}</dd>
                          <dt className="deflist-term">Местоположение</dt>
                          <dd className="deflist-definition">{task.city}</dd>
                        </dl>
                      </span>
                      <span className="col-6">
                        <dl className="deflist">
                          <dt className="deflist-term">Дата начала</dt>
                          <dd className="deflist-definition">19 февраля 2018, 09:00</dd>
                          <dt className="deflist-term">Оплата</dt>
                          <dd className="deflist-definition">Наличными</dd>
                        </dl>
                      </span>
                    </div>

                  </div>
                </div>

              </div>
              <div className="col-md-3">

                <div className="side-block">
                  <button type="button" data-modal="apply" className="btn btn-primary btn-block btn-lg rip modal-trigger">Предложить</button>
                </div>
                <div className="side-block">
                  <div className="row row-sm align-items-center">
                    <div className="col">
                      <div className="profile-name text-left">Порфирий К.</div>
                      <div className="profile-rating">
                        <div className="rating four"></div>
                        <span className="rating-text">(1526 отзывов)</span>
                      </div>
                    </div>
                    <div className="col-auto">
                      <div className="user-image user-image-md"></div>
                    </div>
                  </div>
                </div>
                <div className="side-block">
                  <dl className="deflist">
                    <dt className="deflist-term">Местоположение</dt>
                    <dd className="deflist-definition">Балашиха, Московская область</dd>
                    <dt className="deflist-term">Дата начала</dt>
                    <dd className="deflist-definition">19 февраля 2018, 09:00</dd>
                    <dt className="deflist-term">Бюджет</dt>
                    <dd className="deflist-definition">Мелкий — до 3 000 ₽</dd>
                    <dt className="deflist-term">Оплата</dt>
                    <dd className="deflist-definition">Наличными</dd>
                  </dl>
                </div>

              </div>
              <div className="col-md-9">

                <h2 className="heading h5 text-left">Ответы на задание</h2>

                <div className="panel">
                  <div className="panel-body">
                    <div className="row">
                      <div className="col-2"><a href="profile.html">Владимир М.</a></div>
                      <div className="col">Могу сделать за 6 дней. Я быстрый как молния</div>
                    </div>
                  </div>
                </div>

                <div className="panel">
                  <div className="panel-body">
                    <div className="row">
                      <div className="col-2"><a href="profile.html">Константин И.</a></div>
                      <div className="col">Хорошо монтирую кондиционеры, а в гаражах вобще эксперт</div>
                    </div>
                  </div>
                </div>

                <div className="panel">
                  <div className="panel-body">
                    <div className="row">
                      <div className="col-2"><a href="profile.html">Аристарх П.</a></div>
                      <div className="col">У меня как раз клей и скотч есть. Могу начать прямо завтра</div>
                    </div>
                  </div>
                </div>

              </div>
              <div className="col-md-9">

                <h2 className="heading h5 text-left">Похожие задания</h2>

                <div className="panel">
                  <div className="panel-body">
                    <a className="panel-title" href="tasks-detail.html">Починить кондиционер в гараже</a>
                    <p className="panel-text">I am looking for an experienced wordpress developer to make changes to an existing automated email that is generated in my website when I register a trainer. I require some files attached to the email and a Youtube video inserted into the content ...</p>
                    <div className="panel-props row">
                      <div className="panel-prop col-auto panel-date">26.06.2018 10:12</div>
                      <div className="panel-prop col-auto">Строительство</div>
                      <div className="panel-prop col">просмотров: 689</div>
                      <div className="panel-prop col-auto panel-price">10 680 руб.</div>
                    </div>
                  </div>
                </div>

                <div className="panel">
                  <div className="panel-body">
                    <a className="panel-title" href="tasks-detail.html">Починить кондиционер в гараже</a>
                    <p className="panel-text">I am looking for an experienced wordpress developer to make changes to an existing automated email that is generated in my website when I register a trainer. I require some files attached to the email and a Youtube video inserted into the content ...</p>
                    <div className="panel-props row">
                      <div className="panel-prop col-auto panel-date">26.06.2018 10:12</div>
                      <div className="panel-prop col-auto">Строительство</div>
                      <div className="panel-prop col">просмотров: 689</div>
                      <div className="panel-prop col-auto panel-price">10 680 руб.</div>
                    </div>
                  </div>
                </div>

                <div className="panel">
                  <div className="panel-body">
                    <a className="panel-title" href="tasks-detail.html">Починить кондиционер в гараже</a>
                    <p className="panel-text">I am looking for an experienced wordpress developer to make changes to an existing automated email that is generated in my website when I register a trainer. I require some files attached to the email and a Youtube video inserted into the content ...</p>
                    <div className="panel-props row">
                      <div className="panel-prop col-auto panel-date">26.06.2018 10:12</div>
                      <div className="panel-prop col-auto">Строительство</div>
                      <div className="panel-prop col">просмотров: 689</div>
                      <div className="panel-prop col-auto panel-price">10 680 руб.</div>
                    </div>
                  </div>
                </div>

                <div className="panel">
                  <div className="panel-body">
                    <a className="panel-title" href="tasks-detail.html">Починить кондиционер в гараже</a>
                    <p className="panel-text">I am looking for an experienced wordpress developer to make changes to an existing automated email that is generated in my website when I register a trainer. I require some files attached to the email and a Youtube video inserted into the content ...</p>
                    <div className="panel-props row">
                      <div className="panel-prop col-auto panel-date">26.06.2018 10:12</div>
                      <div className="panel-prop col-auto">Строительство</div>
                      <div className="panel-prop col">просмотров: 689</div>
                      <div className="panel-prop col-auto panel-price">10 680 руб.</div>
                    </div>
                  </div>
                </div>

                <div className="panel">
                  <div className="panel-body">
                    <a className="panel-title" href="tasks-detail.html">Починить кондиционер в гараже</a>
                    <p className="panel-text">I am looking for an experienced wordpress developer to make changes to an existing automated email that is generated in my website when I register a trainer. I require some files attached to the email and a Youtube video inserted into the content ...</p>
                    <div className="panel-props row">
                      <div className="panel-prop col-auto panel-date">26.06.2018 10:12</div>
                      <div className="panel-prop col-auto">Строительство</div>
                      <div className="panel-prop col">просмотров: 689</div>
                      <div className="panel-prop col-auto panel-price">10 680 руб.</div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>

      </main>
    )
  }
}

TaskDetail.propTypes = {
  task: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps)(TaskDetail);
