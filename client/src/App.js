import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import { setCurrentUser, logoutUser } from './actions/authActions';
import { clearCurrentProfile } from './actions/profileActions';
import { Provider } from 'react-redux';
import store from './store';

import PrivateRoute from './components/common/PrivateRoute';

import Header from './components/layout/Header';
import Home from './components/layout/Home';
import Register from './components/auth/Register';
import Login from './components/auth/Login';
import Footer from './components/layout/Footer';
import Dashboard from './components/dashboard/Dashboard';

import Profiles from './components/profiles/Profiles';
import Profile from './components/profile/Profile';
import CreateProfile from './components/create-profile/CreateProfile';
import EditProfile from './components/edit-profile/EditProfile';

import Tasks from './components/tasks/Tasks';
import TaskForm from './components/tasks/TaskForm';
import Task from './components/task/Task';
import About from './components/content/About';

import NotFound from './components/common/NotFound';

// Check for token
if (localStorage.jwtToken) {
  // Set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // Decode token and get user info and exp
  const decoded = jwt_decode(localStorage.jwtToken);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  // Check for expired token
  const currentTime = Date.now() / 1000;
  if(decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());
    // Clear current profile
    store.dispatch(clearCurrentProfile());
    // Redirect to login
    window.location.href = '/login';
  }
}

class App extends Component {
  render() {
    return (
      <Provider store={ store }>
        <Router>
          <div id="page">
            <Header />
            <Switch>
              <Route exact path="/" component={ Home } />
              <Route exact path="/tasks" component={ Tasks } />
              <Route exact path="/tasks/new" component={ TaskForm } />
              <Route exact path="/tasks/:id" component={ Task } />
              <Route exact path="/executors" component={ Profiles } />
              <Route exact path="/profile/:handle" component={ Profile } />
              <Route exact path="/about" component={ About } />
              <Route exact path="/login" component={ Login } />
              <Route exact path="/register" component={ Register } />
              <PrivateRoute exact path="/dashboard" component={ Dashboard } />
              <PrivateRoute exact path="/create-profile" component={ CreateProfile } />
              <PrivateRoute exact path="/edit-profile" component={ EditProfile } />
              <Route exact path="/not-found" component={ NotFound } />
            </Switch>
            <Footer />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
