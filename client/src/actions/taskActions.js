import axios from 'axios';

import {
  ADD_TASK,
  GET_ERRORS,
  GET_TASKS,
  GET_TASK,
  TASK_LOADING
} from './types';

// Add task
export const addTask = taskData => dispatch => {
  axios
    .post('/api/tasks', taskData)
    .then(res =>
      dispatch({
        type: ADD_TASK,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    )
}

// Get tasks
export const getTasks = () => dispatch => {
  dispatch(setTaskLoading());
  axios
    .get('/api/tasks')
    .then(res =>
      dispatch({
        type: GET_TASKS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_TASKS,
        payload: null
      })
    )
}

// Set loading state
export const setTaskLoading = () => {
  return {
    type: TASK_LOADING
  };
};

// Get 
export const getTask = id => dispatch => {
  dispatch(setTaskLoading());
  axios
    .get(`/api/tasks/${id}`)
    .then(res =>
      dispatch({
        type: GET_TASK,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_TASK,
        payload: null
      })
    )
}
