/*
import React, { Component } from 'react';

class Tasks {

}

class Chat {
	constructor(props) {
		super(props);
		this.state = {
			messages: props.messages
		};
	}
	getLastMessage() {
		return this.state.messages[this.state.messages.length - 1];
	}

}

class Settings {

}*/

(function(){
	/*
		var nav = document.getElementById("navigation"),
			toggler = document.getElementById("nav-toggle"),
			closer = document.getElementById("nav-close");
	*/
	var headerHeight = document.getElementById( 'header' ).offsetHeight;

	var App = {

		init: function() {
				/*
				closer.addEventListener("click", this.navClose, false),
				toggler.addEventListener("click", this.navToggle, false),
				window.addEventListener("load", this.navClose, false),
				window.addEventListener("resize", this.navClose, false);
				*/
			window.addEventListener("load", this.pageReady, false);
			window.addEventListener("load", this.modalEffect, false);
			window.addEventListener("load", this.rippleEffect, false);
			window.addEventListener("load", this.checkValidity, false);
		},

		navOpen: function() { nav.classList.add("open") },
		navClose: function() { nav.classList.remove("open") },
		navToggle: function() { nav.classList.toggle("open") },

		getCoords: function(elem) {

			var box = elem.getBoundingClientRect(),
				body = document.body,
				docEl = document.documentElement,
				scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop,
				scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft,
				clientTop = docEl.clientTop || body.clientTop || 0,
				clientLeft = docEl.clientLeft || body.clientLeft || 0,
				top = box.top + scrollTop - clientTop,
				left = box.left + scrollLeft - clientLeft;

			return {
				top: top,
				left: left
			};

		},

		pageReady: function() {

			var page = document.getElementById('page');
			page.classList.add('ready');

		},

    modalEffect: function() {

			var triggers = document.getElementsByClassName('modal-trigger'),
			buttonsRip = Array.prototype.filter.call( triggers, function( trigger ) {

				var modal = document.querySelector( '#' + trigger.getAttribute( 'data-modal' ) ),
						overlay = modal.querySelector( '.modal-overlay' ),
						close = modal.querySelector( '.modal-close' );

				function removeModal() {
					modal.classList.remove( 'modal-show' );
				}

				trigger.addEventListener( 'click', function() {
					modal.classList.add( 'modal-show' );
					overlay.removeEventListener( 'click', removeModal );
					overlay.addEventListener( 'click', removeModal );

					if ( trigger.classList.contains( 'modal-setperspective' ) ) {
						setTimeout( function() {
							document.documentElement.classList.add( 'modal-perspective' );
						}, 25 );
					}
				});

				close.addEventListener( 'click', function( event ) {
					event.stopPropagation();
					removeModal();
				});

			});

    },

		rippleEffect: function() {

			var buttons = document.getElementsByClassName('rip'),
			buttonsRip = Array.prototype.filter.call( buttons, function( btn ) {
				btn.addEventListener('click', function(event) {
					var X = event.pageX - App.getCoords(this).left,
						Y = event.pageY - App.getCoords(this).top,
						customColor = this.getAttribute('data-ripple'),
						rippleDiv = document.createElement("div");
					rippleDiv.className = 'ripple';
					rippleDiv.setAttribute("style","top:"+Y+"px; left:"+X+"px;");
					if (customColor) rippleDiv.style.background = customColor;
					this.appendChild(rippleDiv);
					setTimeout(function(){
						rippleDiv.parentElement.removeChild(rippleDiv);
					}, 1000);
				}, false);
			});

		},

		checkValidity: function() {

			var forms = document.getElementsByClassName('needs-validation'),
			validation = Array.prototype.filter.call(forms, function(form) {
				form.addEventListener('submit', function(event) {
					if (form.checkValidity() === false) {
						event.preventDefault();
						event.stopPropagation();
					}
					form.classList.add('was-validated');
				}, false);
				Array.prototype.filter.call(form.elements, function(el) {
					function checkForm() {
						if (form.checkValidity() === true) {
							form.classList.add('is-valid');
						} else {
							form.classList.remove('is-valid');
						}
					}
					el.addEventListener('change', checkForm, false);
					el.addEventListener('keydown', checkForm, false);
					el.addEventListener('keypress', checkForm, false);
					el.addEventListener('keyup', checkForm, false);
					el.addEventListener('paste', checkForm, false);
					el.addEventListener('cut', checkForm, false);
				});

			});

		}

	};
	App.init();

})();