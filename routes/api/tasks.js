const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const passport = require('passport');

// task model
const Task = require('../../models/Task');
const Profile = require('../../models/Profile');

// Validation
const validateTaskInput = require('../../validation/task');

// @route GET api/tasks/test
// @desc Tests task route
// @access Public
router.get('/test', (req, res) => res.json({msg: "Tasks works"}));

// @route GET api/tasks
// @desc Get tasks
// @access Public
router.get(
  '/',
  (req, res) => {
    Task.find()
      .sort({ date: -1 })
      .then(tasks => res.json(tasks))
      .catch(err => res.status(404).json({ notasksfound: 'No tasks found' }));
  }
);

// @route GET api/tasks/:id
// @desc Get task by id
// @access Public
router.get(
  '/:id',
  (req, res) => {
    Task.findById(req.params.id)
      .sort({ date: -1 })
      .then(task => res.json(task))
      .catch(err => res.status(404).json({ notaskfound: 'No task found with that ID' }));
  }
);

// @route POST api/tasks
// @desc Create task
// @access Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateTaskInput(req.body);

    // Check validation
    if (!isValid) return res.status(400).json(errors);

    const newTask = new Task({
      user: req.user.id,
      name: req.body.name,
      avatar: req.body.avatar,
      title: req.body.title,
      category: req.body.category,
      subcategory: req.body.subcategory,
      price: req.body.price,
      deadline: req.body.deadline,
      city: req.body.city,
      address: req.body.address,
      content: req.body.content
    });

    newTask.save().then(task => res.json(task));
  }
);

// @route DELETE api/tasks/:id
// @desc Delete task
// @access Private
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Profile.findOne({ user: req.user.id })
      .then(profile => {
        Task.findById(req.params.id)
          .then(task => {
            if (task.user.toString() !== req.user.id) {
              return res.status(401).json({ notauthorized: 'User not authorized' })
            }

            // Delete
            task.remove().then(() => res.json({ success: true }));
          })
          .catch(err => res.status(404).json({ tasknotfound: 'No task found' }));
      }
    );
  }
);

// @route POST api/tasks/view/:id
// @desc Task is viewed
// @access Private
router.post(
  '/view/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Profile.findOne({ user: req.user.id })
      .then(profile => {
        Task.findById(req.params.id)
          .then(task => {
            if (task.views.filter(view => view.user.toString() === req.user.id).length > 0) {
              return res.status(400).json({ alreadyviewed: 'User already viewed this task' });
            }

            // Add user id to views array
            task.views.unshift({ user: req.user.id });

            task.save().then(task => res.json(task));
          })
          .catch(err => res.status(404).json({ tasknotfound: 'No task found' }));
      }
    );
  }
);

// @route POST api/tasks/comment/:id
// @desc Add comment to task
// @access Private
router.post(
  '/comment/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateTaskInput(req.body);

    // Check validation
    if (!isValid) return res.status(400).json(errors);

    Task.findById(req.params.id)
      .then(task => {

        const newComment = {
          user: req.user.id,
          name: req.body.name,
          avatar: req.body.avatar,
          text: req.body.text
        }

        // Add to applications
        task.comments.unshift(newComment);

        // Save
        task.save().then(task => res.json(task));
      })
      .catch(err => res.status(404).json({ tasknotfound: 'No task found' }));
  }
);

// @route DELETE api/tasks/comment/:id/:comment_id
// @desc Remove comment from task
// @access Private
router.delete(
  '/comment/:id/:comment_id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Task.findById(req.params.id)
      .then(task => {

        // Check if comment exists
        if (task.comments.filter(comment => comment._id.toString() === req.params.comment_id).length === 0) {
          return res.status(404).json({ commentnotexists: 'Comment does not exists' });
        }

        // Get remove index
        const removeIndex = task.comments
          .map(item => item._id.toString())
          .indexOf(req.params.comment_id);

        // Splice comment out of array
        task.comments.splice(removeIndex, 1);

        // Save
        task.save().then(task => res.json(task));
      })
      .catch(err => res.status(404).json({ tasknotfound: 'No task found' }));
  }
);



module.exports = router;